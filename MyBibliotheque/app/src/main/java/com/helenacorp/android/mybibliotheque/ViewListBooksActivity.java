package com.helenacorp.android.mybibliotheque;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ViewListBooksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_list_books);
    }
}
